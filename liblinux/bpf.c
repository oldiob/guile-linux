/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Copyright (c) 2021 Olivier Dion <olivier.dion@polymtl.ca>
 *
 * See bpf(2) and FreeBSD bpf(4).
 *
 * linux/bpf/bpf.c -
 */

#include <linux/bpf.h>
#include <syscall.h>
#include <unistd.h>

#include "common.c"

int bpf(int cmd, union bpf_attr *attr, unsigned int size)
{
	return syscall(SYS_bpf, cmd, attr, size);
}

void init_liblinux_bpf(void)
{
	DEF_INT(BPF_LD);
	DEF_INT(BPF_W);
	DEF_INT(BPF_ABS);
	DEF_INT(BPF_K);
	DEF_INT(BPF_RET);

	DEF_INT(BPF_JMP);
	DEF_INT(BPF_JA);
	DEF_INT(BPF_JEQ);
	DEF_INT(BPF_JGT);
	DEF_INT(BPF_JGE);
	DEF_INT(BPF_JSET);
}
