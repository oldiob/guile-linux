/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Copyright (c) 2021 Olivier Dion <olivier.dion@polymtl.ca>
 */

#include <sys/prctl.h>
#include <syscall.h>
#include <unistd.h>

#include "common.c"

void init_liblinux_prctl(void)
{
	DEF_INT(PR_SET_NO_NEW_PRIVS);
}
