(define-module (linux seccomp action)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 match)
  #:use-module (linux seccomp bindings)
  #:use-module (system foreign))

(define (action->uint32 action)
  (match action
    ('kill-process SECCOMP_RET_KILL_PROCESS)
    ('kill-thread  SECCOMP_RET_KILL_THREAD)
    ('trap         SECCOMP_RET_TRAP)
    ('errno        SECCOMP_RET_ERRNO)
    ('user-notif   SECCOMP_RET_USER_NOTIF)
    ('trace        SECCOMP_RET_TRACE)
    ('log          SECCOMP_RET_LOG)
    ('allow        SECCOMP_RET_ALLOW)
    (other         (throw 'invalid-action other))))

;; Determine if ACTION is available for BPF filter.  Returns 0 if ACTION is
;; available, otherwise -1 and `errno` is set to EOPNOTSUPP.  Valid values for
;; ACTION are the symbols: '(allow errno kill-process kill-thread log trace
;; trap). Throw 'invalid-action if ACTION is not valid.
(define-public (syscall/seccomp:action action)
  "In theory, the system call could return EINVAL or EFAULT.
But in practice, this wrapper ensure that this can not happen."
  (receive (ret errno)
        (syscall/seccomp SECCOMP_GET_ACTION_AVAIL 0
                   (make-c-struct (list uint32)
                                  (list (action->uint32 action))))
    ret))
