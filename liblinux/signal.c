/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Copyright (c) 2021 Olivier Dion <olivier.dion@polymtl.ca>
 */

#include <signal.h>

#include "common.c"

void init_liblinux_signal(void)
{
	DEF_INT(SIGSYS);
	DEF_INT(SIGKILL);
}
