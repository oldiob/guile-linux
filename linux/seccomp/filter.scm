(define-module (linux seccomp filter)
  #:use-module (ice-9 match)
  #:use-module (linux bpf)
  #:use-module (linux bpf bindings)
  #:use-module (linux seccomp bindings)
  #:export (syscall/seccomp:filter))

(define (load what)
  (make-instruction (logior BPF_LD BPF_W BPF_ABS)
                    0 0
                    what))

(define* (jump pred if-true if-false #:optional (konstant 0))
  (make-instruction (logior BPF_JMP BPF_K pred)
                    if-true
                    if-false
                    konstant))

(define (ret how)
  (make-instruction (logior BPF_RET BPF_K)
                    0 0
                    how))

(define (make-bpf-program program)
  (make-program
   (list->vector (map (match-lambda
                        ('ld:syscall (load offsetof/struct-seccomp-data:nr))
                        ('ld:arch    (load offsetof/struct-seccomp-data:arch))
                        ('ld:ip      (load offsetof/struct-seccomp-data:ip))
                        ('ld:arg0    (load offsetof/struct-seccomp-data:arg0))
                        ('ld:arg1    (load offsetof/struct-seccomp-data:arg1))
                        ('ld:arg2    (load offsetof/struct-seccomp-data:arg2))
                        ('ld:arg3    (load offsetof/struct-seccomp-data:arg3))
                        ('ld:arg4    (load offsetof/struct-seccomp-data:arg4))
                        ('ld:arg5    (load offsetof/struct-seccomp-data:arg5))

                        (('jmp to)        (jump BPF_JA  to to 0))
                        (('jmp:eq  k to)  (jump BPF_JEQ to 0 k))
                        (('jmp:neq k to)  (jump BPF_JEQ 0 to k))
                        (('jmp:gt  k to)  (jump BPF_JGT to 0 k))
                        (('jmp:lt  k to)  (jump BPF_JGE 0 to k))
                        (('jmp:ge  k to)  (jump BPF_JGE to 0 k))
                        (('jmp:le  k to)  (jump BPF_JGT 0 to k))
                        (('jmp:set k t f) (jump BPF_JSET t f k))

                        ('kill:proc   (ret SECCOMP_RET_KILL_PROCESS))
                        ('kill:thread (ret SECCOMP_RET_KILL_THREAD))
                        (('trap k)    (ret (logior SECCOMP_RET_TRAP k)))
                        (('errno err) (ret (logior SECCOMP_RET_ERRNO err)))
                        ('user-notif  (ret SECCOMP_RET_USER_NOTIF))
                        (('trace k)   (ret (logior SECCOMP_RET_TRACE k)))
                        ('log         (ret SECCOMP_RET_LOG))
                        ('allow       (ret SECCOMP_RET_ALLOW)))
                      program))))

(define* (syscall/seccomp:filter program #:optional (options #nil))
  (let* ((flags (apply logior
                      (map (match-lambda
                             ('log          SECCOMP_FILTER_FLAG_LOG)
                             ('new-listener SECCOMP_FILTER_FLAG_NEW_LISTENER)
                             ('spec-allow   SECCOMP_FILTER_FLAG_SPEC_ALLOW)
                             ('tsync        SECCOMP_FILTER_FLAG_TSYNC))
                           options))))
    (syscall/seccomp SECCOMP_SET_MODE_FILTER flags (make-bpf-program program))))
