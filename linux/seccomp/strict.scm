(define-module (linux seccomp strict)
  #:use-module (linux seccomp bindings)
  #:use-module (system foreign))

(define-public (syscall/seccomp:strict)
  (syscall/seccomp SECCOMP_SET_MODE_STRICT 0 %null-pointer))
