(define-module (linux prctl bindings)
  #:use-module (system foreign-library)
  #:use-module (system foreign)
  #:export (syscall/prctl
            PR_SET_NO_NEW_PRIVS))

(define syscall/prctl
  (eval-when (eval load compile)
    (let ((this-lib (load-foreign-library "liblinux")))
      ((foreign-library-function this-lib "init_liblinux_prctl"))
      (foreign-library-function #f "prctl"
                                #:return-type int
                                #:arg-types (list int
                                                  unsigned-long unsigned-long
                                                  unsigned-long unsigned-long)
                                #:return-errno? #t))))
