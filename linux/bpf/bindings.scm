(define-module (linux bpf bindings)
  #:use-module (linux)
  #:use-module (system foreign-library)
  #:use-module (system foreign)
  #:export (syscall/bpf
            BPF_LD
	    BPF_W
	    BPF_ABS
            BPF_K
            BPF_RET
	    BPF_JMP
	    BPF_JA
	    BPF_JEQ
	    BPF_JGT
	    BPF_JGE
	    BPF_JSET))

(define syscall/bpf
  (eval-when (eval load compile)
    (let ((this-lib (load-foreign-library "liblinux")))
      ((foreign-library-function this-lib "init_liblinux_bpf"))
      (foreign-library-function this-lib "bpf"
                                #:return-type int
                                #:arg-types (list int '* unsigned-int)
                                #:return-errno? #t))))
