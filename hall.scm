(hall-description
  (name "linux")
  (prefix "guile")
  (version "0.1")
  (author "Olivier Dion")
  (copyright (2021))
  (synopsis "")
  (description "")
  (home-page "")
  (license gpl3+)
  (dependencies `())
  (files (libraries
           ((scheme-file "linux")
            (directory
              "linux"
              ((directory "bpf" ((scheme-file "bindings")))
               (directory "prctl" ((scheme-file "bindings")))
               (directory
                 "seccomp"
                 ((scheme-file "strict")
                  (scheme-file "filter")
                  (scheme-file "bindings")
                  (scheme-file "unotify")
                  (scheme-file "action")))
               (scheme-file "seccomp")
               (scheme-file "bpf")))))
         (tests ((directory
                  "tests"
                  ((scheme-file "seccomp")))))
         (programs ((directory "scripts" ())))
         (documentation
           ((org-file "README")
            (symlink "README" "README.org")
            (text-file "HACKING")
            (text-file "COPYING")
            (directory "doc" ((texi-file "linux")))))
         (infrastructure
           ((shell-file "bootstrap")
            (scheme-file "hall")
            (scheme-file "guix")
            (shell-file "patch-autotools")
            (directory
              "liblinux"
              ((c-file "seccomp")
               (c-file "bpf")
               (c-file "prctl")
               (c-file "signal")
               (c-file "common")))))))
