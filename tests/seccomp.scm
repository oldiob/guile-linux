(define-module (tests seccomp)
  #:use-module (ice-9 receive)
  #:use-module (linux seccomp action)
  #:use-module (linux seccomp strict)
  #:use-module (linux seccomp filter)
  #:use-module (linux seccomp bindings)
  #:use-module (linux prctl bindings)
  #:use-module (srfi srfi-64))

;; TODO - Define this in liblinux
(define SIGKILL 9)
(define SIGSYS 31)
(define SYS_write 1)

;; Helpers
(define (with-fork child-do parent-do)
  (let ((cpid (primitive-fork)))
    (when (= 0 cpid)
      (child-do)
      (primitive-exit 0))
    (parent-do cpid)))

(define (wait-for-child child-do parent-do)
  (let ((cpid (primitive-fork)))
    (when (= 0 cpid)
      (child-do)
      (primitive-exit 0))
    (parent-do (cdr (waitpid cpid)))))

(test-begin "seccomp")

(test-group "action"
            (for-each (lambda (action)
                        (test-eq 0 (syscall/seccomp:action action)))
                      '(kill-process kill-thread trap errno user-notif trace log allow))

            (test-error 'invalid-action (syscall/seccomp:action 'not-an-action)))

(test-group "strict"
            ;; Ok so here we want to test the strict mode in a children process, otherwise
            ;; we're shooting ourself in the foot.
            ;;
            ;; NOTE!  We're expecting the child process to get killed by SIGKILL (see the
            ;; DESCRIPTION section of seccomp(2)) because glibc will call exit_group(2) and
            ;; not _exit(2).
            (with-fork
             (lambda ()
               (syscall/seccomp:strict)
               (primitive-exit 0))
             (lambda (cpid)
               (test-eq SIGKILL (status:term-sig (cdr (waitpid cpid)))))))

(test-group "filter"
            ;; We need to enable the PR_SET_NO_NEW_PRIVS bit, otherwise no filter can be
            ;; installed.  This bit is inherited by the children of this process.
            (receive (ret errno)
                (syscall/prctl PR_SET_NO_NEW_PRIVS 1 0 0 0)
              (test-eq 0 (status:exit-val ret)))

            ;; Test blocking all system call.
            (wait-for-child
             (lambda () (syscall/seccomp:filter '(kill:proc)))
             (lambda (status) (test-eq SIGSYS (status:term-sig status))))

            ;; Test blocking only write(2).
            (wait-for-child
             (lambda ()
               (receive (ret errno)
                   (syscall/seccomp:filter
                    `(ld:syscall
                      (jmp:neq ,SYS_write 1)
                      kill:proc
                      allow))
                 (format #t "THIS IS A BUG: ~a - ~a" errno (strerror errno))))
             (lambda (status) (test-eq SIGSYS (status:term-sig status))))

            ;; Test blocking only write(2), but not actually writing anything.
            (wait-for-child
             (lambda () (syscall/seccomp:filter
                    `(ld:syscall
                      (jmp:neq ,SYS_write 1)
                      kill:proc
                      allow)))
             (lambda (status) (test-eq 0 (status:exit-val status))))

            ;; Let's set a filter that block setting other filter by retuning a random errno.
            (let ((expected-error (random 256)))
              (wait-for-child
               (lambda ()
                 (syscall/seccomp:filter
                  `(ld:syscall
                    (jmp:neq ,SYS_seccomp 1)
                    (errno ,expected-error)
                    allow))
                 (receive (ret errno)
                     (syscall/seccomp:strict)
                   (exit errno))
                 )

               (lambda (status) (test-eq expected-error (status:exit-val status))))))

(test-end "seccomp")
