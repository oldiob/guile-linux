#!/bin/sh

if [ -z "${GUIX_ENVIRONMENT}" ]
then
    exec guix environment --load=./guix.scm -- sh $0
fi

hall dist -x
./patch-autotools.sh
autoreconf -vfi
