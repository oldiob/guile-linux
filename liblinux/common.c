/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Copyright (c) 2021 Olivier Dion <olivier.dion@polymtl.ca>
 *
 * NOTE!  This is a header file, even though it has a C extension.  This is
 * because Hall does not recognize C header file yet.
 */

#include <libguile.h>

#define DEF_INT(NAME)		scm_c_define(#NAME, scm_from_int(NAME))
#define DEF_UINT(NAME)		scm_c_define(#NAME, scm_from_uint(NAME))
#define DEF_UINT32(NAME)	scm_c_define(#NAME, scm_from_uint32(NAME))
