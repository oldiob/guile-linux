(define-module (linux seccomp)

  #:use-module (ice-9 receive)
  #:use-module (oop goops)
  #:use-module (linux seccomp strict)
  #:use-module (linux seccomp action)
  #:use-module (linux seccomp filter)
  #:use-module (linux seccomp unotify)

  #:re-export ((syscall/seccomp:action . action-available?)
               (syscall/seccomp:strict . enter-strict-computing-mode))

  #:export (<secccomp>
            install-filter))

(define* (install-filter program #:optional (options #nil))
  (receive (ret errno)
      (syscall/seccomp:filter program options)
    (cond
     ((member 'tsync options)
      (if (= 0 ret)
          0
          (throw 'tsync errno)))
     ((member 'new-listener options)
      (if (>= 0 ret)
          ret
          (scm-error 'system-error "seccomp()" "~A"
                     (strerror errno) #f)))
     (else
      (if (= 0 ret)
          0
          (scm-error 'system-error "seccomp()" "~A"
                     (strerror errno) #f))))))

;; (define* (install-filter-and-monitor program thunk #:optional fork?)
;;   (if fork?
;;       (install-filter-and-monitor/process program thunk)
;;       (install-filter-and-monitor/thread)))

(define (throw-errno syscall errno)
  (scm-error 'system-error syscall "~A" (strerror errno) (list errno)))

(define-class <seccomp-notify-response> ()
  (context #:init-keyword #:ctx)
  (request #:init-keyword #:req))

(define-method (send (res <seccomp-notify-response>) payload)
  (let ((cookie (car (slot-ref res 'request)))
        (fd (slot-ref (slot-ref res 'context) 'file-descriptor)))
    (syscall/ioctl:seccomp-send fd (cons cookie payload))))

(define-method (accept <res <seccomp-notify))

(define-class <seccomp-notify> ()
  (file-descriptor #:init-keyword #:fd))

;; TODO - I want this to be call if the object is GC.  Probably have to do this
;; in C.
(define-method (close (ctx <seccomp-notify>))
  (let ((fd (slot-ref ctx 'file-descriptor)))
    (when (>= fd 0)
      (close fd)
      (slot-set! ctx 'file-descriptor -1))))

(define-method (wait (ctx <seccomp-notify>) response-proc)
  (receive (ret errno request)
      (syscall/ioctl:seccomp-recv (slot-ref ctx 'file-descriptor))
    (unless (= 0 ret)
      (throw-errno "ioctl(SECCOMP_IOCTL_NOTIF_RECV)" errno))
    (response-proc (make <seccomp-notify-response> #:ctx ctx #:req request))))
