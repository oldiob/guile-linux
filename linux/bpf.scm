(define-module (linux bpf)
  #:use-module (srfi srfi-43)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)
  #:use-module (system foreign))

;;; For more informations on the BPF machine:
;;;
;;; - linux/Documentation/networking/filter.txt
;;; - bpf(4) (FreeBSD)

(define write-c-struct (@@ (system foreign) write-c-struct))

(define struct:sock-fprog (list unsigned-short '*))
(define struct:sock-filter (list uint16 uint8 uint8 uint32))

(define struct:sock-fprog/sizeof (sizeof struct:sock-fprog))
(define struct:sock-filter/sizeof (sizeof struct:sock-filter))

(define (make-filter% instructions)
  (let* ((instruction-count (vector-length instructions))
         (raw-instructions (make-bytevector (* instruction-count struct:sock-filter/sizeof) 0)))

    (vector-for-each (lambda (index instruction)
                       (write-c-struct
                        raw-instructions
                        (* index struct:sock-filter/sizeof)
                        struct:sock-filter
                        (parse-c-struct instruction struct:sock-filter)))
                     instructions)


    (make-c-struct struct:sock-fprog
                   (list instruction-count
                         (bytevector->pointer raw-instructions)))))

(define-public (make-instruction code jump-if-true jump-if-false konstant)
  (make-c-struct struct:sock-filter
                 (list code jump-if-true jump-if-false konstant)))

(define-public (make-program instructions)
  (if (vector? instructions)
      (make-filter% instructions)
      (throw 'bad-filter "INSTRUCTIONS must be a vector")))
