#!/bin/sh

SOURCES=$(find liblinux -name "*.c" -printf '%p ')

mkdir -p build-aux
touch build-aux/config.rpath

sed -i  -e 's|exec "\$@"|if test "$LTDL_LIBRARY_PATH" = ""; then\n  LTDL_LIBRARY_PATH=@abs_top_builddir@/.libs\nelse\n  LTDL_LIBRARY_PATH=@abs_top_builddir@/.libs:$LTDL_LIBRARY_PATH\nfi\nexport LTDL_LIBRARY_PATH\n\nexec "$@"|g' pre-inst-env.in

sed -i                                                                            \
    -e 's|GUILE_PROGS|GUILE_PROGS\nGUILE_FLAGS|g'                                 \
    -e 's|AC_OUTPUT|AC_CONFIG_MACRO_DIRS([m4])\nAC_PROG_CC\nLT_INIT\nAC_OUTPUT|g' \
    configure.ac

echo 'extlibdir = $(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/extensions' >> Makefile.am
echo 'extlib_LTLIBRARIES = liblinux.la' >> Makefile.am
echo 'AM_CFLAGS = -I$(srcdir) $(WARN_CFLAGS) $(DEBUG_CFLAGS)' >> Makefile.am
echo "liblinux_la_SOURCES = $SOURCES" >> Makefile.am
echo 'liblinux_la_CFLAGS = $(AM_CFLAGS) $(GUILE_CFLAGS)' >> Makefile.am
echo 'liblinux_la_LIBADD = $(GUILE_LIBS)' >> Makefile.am
echo 'liblinux_la_LDFLAGS = -export-dynamic -module' >> Makefile.am
echo '$(GOBJECTS): liblinux.la' >> Makefile.am
