(define-module (linux seccomp unotify)
  #:use-module (ice-9 receive)
  #:use-module (linux seccomp bindings)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1))

(define struct-notif-response
  (let ((ABIs (list

               ;; ABI 1
               (list
                uint64  ;; id
                int64   ;; val
                int32   ;; error
                uint32) ;; flags
               )))

    (find (lambda (ABI)
            (= sizeof/struct-seccomp-notif-resp (sizeof ABI)))
          ABIs)))

(define struct-data
  (let ((ABIs (list

               ;; ABI 1
               (list
                int                                  ;; nr
                uint32                               ;; arch
                uint64                               ;; instruction_pointer
                (list uint64 uint64 uint64           ;; system call arguments
                      uint64 uint64 uint64))

               )))

    (find (lambda (ABI)
            (= sizeof/struct-seccomp-data (sizeof ABI)))
          ABIs)))

(define struct-notif-request
   (let ((ABIs (list

                ;; ABI 1
                (list
                 uint64                 ;; id
                 uint32                 ;; pid
                 uint32                 ;; flags
                 struct-data)           ;; data

               )))

    (find (lambda (ABI)
            (= sizeof/struct-seccomp-notif (sizeof ABI)))
          ABIs)))

(define ioctl
  (pointer->procedure int
                      (dynamic-func "ioctl" (dynamic-link))
                      (list int int '*)
                      #:return-errno? #t))

(define-public (syscall/ioctl:seccomp-recv fd)
  (let* ((request (make-bytevector sizeof/struct-seccomp-notif-resp 0)))
    (receive (ret errno)
        (ioctl fd SECCOMP_IOCTL_NOTIF_RECV request)
      (values ret errno (parse-c-struct request struct-notif-request)))))

(define-public (syscall/ioctl:seccomp-send fd response)
  (ioctl fd SECCOMP_IOCTL_NOTIF_SEND (make-c-struct struct-notif-response
                                                    response)))

(define-public (syscall/ioctl:seccomp-valid fd ID)
  (ioctl fd SECCOMP_IOCTL_NOTIF_ID_VALID (make-c-struct (list uint64)
                                                        (list ID))))

;; (define-public (syscall/ioctl:seccomp-addfd fd . args)
;;   (ioctl fd SECCOMP_IOCTL_NOTIF_ADDFD (make-c-struct struct-addfd args)))
