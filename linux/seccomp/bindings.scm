;; SPDX-License-Identifier: GPL-3.0-or-later
;;
;; Copyright (c) 2021 Olivier Dion <olivier.dion@polymtl.ca>
;;
;; linux/seccomp/bindings.scm - Guile's module bindings to seccomp(2).

(define-module (linux seccomp bindings)
  #:use-module (linux)
  #:use-module (system foreign-library)
  #:use-module (system foreign)
  #:export (syscall/seccomp

            sizeof/struct-seccomp-notif
            sizeof/struct-seccomp-notif-resp
            sizeof/struct-seccomp-data

            offsetof/struct-seccomp-data:nr
	    offsetof/struct-seccomp-data:arch
	    offsetof/struct-seccomp-data:ip
	    offsetof/struct-seccomp-data:arg0
	    offsetof/struct-seccomp-data:arg1
	    offsetof/struct-seccomp-data:arg2
	    offsetof/struct-seccomp-data:arg3
	    offsetof/struct-seccomp-data:arg4
	    offsetof/struct-seccomp-data:arg5

            SYS_seccomp

            SECCOMP_SET_MODE_STRICT
	    SECCOMP_SET_MODE_FILTER
	    SECCOMP_GET_ACTION_AVAIL
	    SECCOMP_GET_NOTIF_SIZES

	    SECCOMP_FILTER_FLAG_LOG
	    SECCOMP_FILTER_FLAG_NEW_LISTENER
	    SECCOMP_FILTER_FLAG_SPEC_ALLOW
	    SECCOMP_FILTER_FLAG_TSYNC

	    SECCOMP_RET_KILL_PROCESS
	    SECCOMP_RET_KILL_THREAD
	    SECCOMP_RET_KILL
	    SECCOMP_RET_TRAP
	    SECCOMP_RET_ERRNO
	    SECCOMP_RET_USER_NOTIF
	    SECCOMP_RET_TRACE
	    SECCOMP_RET_LOG
	    SECCOMP_RET_ALLOW

	    SECCOMP_IOCTL_NOTIF_RECV
	    SECCOMP_IOCTL_NOTIF_SEND
	    SECCOMP_IOCTL_NOTIF_ID_VALID
	    SECCOMP_IOCTL_NOTIF_ADDFD))


(define syscall/seccomp
  (eval-when (eval load compile)
    (let ((this-lib (load-foreign-library "liblinux")))
      ((foreign-library-function this-lib "init_liblinux_seccomp"))
      (foreign-library-function this-lib "seccomp"
                                #:return-type int
                                #:arg-types (list int unsigned-int '*)
                                #:return-errno? #t))))
