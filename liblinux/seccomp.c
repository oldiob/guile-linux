/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Copyright (c) 2021 Olivier Dion <olivier.dion@polymtl.ca>
 *
 * See seccomp(2) and seccomp_unotify(2).
 *
 * linux/seccomp/seccomp.c -
 */

/* As in seccomp(2) */
#include <linux/audit.h>    /* Definition of AUDIT_* constants */
#include <linux/filter.h>   /* Definition of struct sock_fprog */
#include <linux/seccomp.h>  /* Definition of SECCOMP_* constants */
#include <sys/ioctl.h>      /* Definition of SECCOMP_IOCTL_* constants */
#include <sys/ptrace.h>     /* Definition of PTRACE_* constants */
#include <sys/syscall.h>    /* Definition of SYS_* constants */
#include <unistd.h>

#include "common.c"

static void init_operations(void);
static void init_flags(void);
static void init_ret(void);
static void init_ioctl(void);
static void init_sizes(void);
static void init_struct_data(void);

int seccomp(unsigned int operation, unsigned int flags, void *args)
{
	int err = syscall(SYS_seccomp, operation, flags, args);

	return err;
}

void init_liblinux_seccomp(void)
{
	DEF_INT(SYS_seccomp);

	init_operations();
	init_flags();
	init_ret();
	init_ioctl();
	init_sizes();
	init_struct_data();
}

static void  init_operations(void)
{
	DEF_UINT(SECCOMP_SET_MODE_STRICT);
	DEF_UINT(SECCOMP_SET_MODE_FILTER);
	DEF_UINT(SECCOMP_GET_ACTION_AVAIL);
	DEF_UINT(SECCOMP_GET_NOTIF_SIZES);
}

static void init_flags(void)
{
	DEF_UINT(SECCOMP_FILTER_FLAG_LOG);
	DEF_UINT(SECCOMP_FILTER_FLAG_NEW_LISTENER);
	DEF_UINT(SECCOMP_FILTER_FLAG_SPEC_ALLOW);
	DEF_UINT(SECCOMP_FILTER_FLAG_TSYNC);
}

static void init_ret(void)
{
	DEF_UINT32(SECCOMP_RET_KILL_PROCESS);
	DEF_UINT32(SECCOMP_RET_KILL_THREAD);
	DEF_UINT32(SECCOMP_RET_KILL);
	DEF_UINT32(SECCOMP_RET_TRAP);
	DEF_UINT32(SECCOMP_RET_ERRNO);
	DEF_UINT32(SECCOMP_RET_USER_NOTIF);
	DEF_UINT32(SECCOMP_RET_TRACE);
	DEF_UINT32(SECCOMP_RET_LOG);
	DEF_UINT32(SECCOMP_RET_ALLOW);
}

static void init_ioctl(void)
{
	DEF_UINT(SECCOMP_IOCTL_NOTIF_RECV);
	DEF_UINT(SECCOMP_IOCTL_NOTIF_SEND);
	DEF_UINT(SECCOMP_IOCTL_NOTIF_ID_VALID);
//	DEF_UINT(SECCOMP_IOCTL_NOTIF_ADDFD);
}

static void init_sizes(void)
{
	struct seccomp_notif_sizes sizes;

	/*
	 * seccomp(SECCOMP_GET_NOTIF_SIZES, flags, buf) can only fail if
	 * `flags != 0` and if `buf` is not mapped in the process
	 * address space.  Thus, we don't need to check the returned
	 * value here because both cases are impossible.
	 */
	(void) seccomp(SECCOMP_GET_NOTIF_SIZES, 0, &sizes);

	/*
	 * These are not constant because they can change from versions of the kernel.
	 */
	scm_c_define("sizeof/struct-seccomp-notif",      scm_from_uint16(sizes.seccomp_notif));
	scm_c_define("sizeof/struct-seccomp-notif-resp", scm_from_uint16(sizes.seccomp_notif_resp));
	scm_c_define("sizeof/struct-seccomp-data",       scm_from_uint16(sizes.seccomp_data));
}

static void init_struct_data(void)
{
	scm_c_define("offsetof/struct-seccomp-data:nr",   scm_from_size_t(offsetof(struct seccomp_data, nr)));
	scm_c_define("offsetof/struct-seccomp-data:arch", scm_from_size_t(offsetof(struct seccomp_data, arch)));
	scm_c_define("offsetof/struct-seccomp-data:ip",   scm_from_size_t(offsetof(struct seccomp_data, instruction_pointer)));
	scm_c_define("offsetof/struct-seccomp-data:arg0", scm_from_size_t(offsetof(struct seccomp_data, args[0])));
	scm_c_define("offsetof/struct-seccomp-data:arg1", scm_from_size_t(offsetof(struct seccomp_data, args[1])));
	scm_c_define("offsetof/struct-seccomp-data:arg2", scm_from_size_t(offsetof(struct seccomp_data, args[2])));
	scm_c_define("offsetof/struct-seccomp-data:arg3", scm_from_size_t(offsetof(struct seccomp_data, args[3])));
	scm_c_define("offsetof/struct-seccomp-data:arg4", scm_from_size_t(offsetof(struct seccomp_data, args[4])));
	scm_c_define("offsetof/struct-seccomp-data:arg5", scm_from_size_t(offsetof(struct seccomp_data, args[5])));
}
